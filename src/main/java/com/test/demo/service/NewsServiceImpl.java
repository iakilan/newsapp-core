package com.test.demo.service;

import com.test.demo.dto.NewsResponse;
import com.test.demo.dto.UpdateCommentsRequest;
import com.test.demo.model.News;
import com.test.demo.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    NewsRepository newsRepository;

    @Override
    public List<NewsResponse> getAllNews() {
        return newsRepository.getAllNews()
                .stream()
                .map(news -> new NewsResponse(news.getId(), news.getUrl(), news.getProfileUrl(), news.getUserName(), news.getContent(), news.getVote(), news.getComments().size()))
                .collect(Collectors.toList());
    }

    @Override
    public News getNewsById(String newsId) {
        return newsRepository.getNewsById(newsId);
    }

    @Override
    public News addNews(News news) {
        return newsRepository.addNews(news);
    }

    @Override
    public News updateComment(String newsId, UpdateCommentsRequest updateCommentsRequest) {
        News existingNews = getNewsById(newsId);
        News.Comment comment = new News.Comment(updateCommentsRequest.getUserName(), updateCommentsRequest.getComment(), updateCommentsRequest.getVote(), updateCommentsRequest.getPostedTime());
        existingNews.getComments().add(comment);
        return addNews(existingNews);
    }
}
