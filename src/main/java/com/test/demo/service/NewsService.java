package com.test.demo.service;

import com.test.demo.dto.NewsResponse;
import com.test.demo.dto.UpdateCommentsRequest;
import com.test.demo.model.News;

import java.util.List;


public interface NewsService {

    /**
     *
     * @return
     */
    List<NewsResponse> getAllNews();

    /**
     *
     * @param newsId
     * @return
     */
    News getNewsById(String newsId);

    /**
     *
     * @param news
     * @return
     */
    News addNews(News news);

    /**
     *
     * @param newsId
     * @param updateCommentsRequest
     * @return
     */
    News updateComment( String newsId, UpdateCommentsRequest updateCommentsRequest);

}
