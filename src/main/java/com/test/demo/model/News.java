package com.test.demo.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.InstantDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.InstantSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Document
public class News {

    @Id
    private String id;
    private String url;
    private String profileUrl;
    private String content;
    private String userName;
    private Long vote;
    private List<Comment> comments;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Comment {
        private String userName;
        private String comment;
        private Long vote;
        //@JsonDeserialize(using = InstantDeserializer.class)
        @JsonSerialize(using = InstantSerializer.class)
        private Instant postedTime;
    }
}
