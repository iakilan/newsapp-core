package com.test.demo.repository;

import com.test.demo.model.News;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


//Added just initial data load.
@Repository
public interface NewsRepositoryTest extends MongoRepository<News, String> {

}
