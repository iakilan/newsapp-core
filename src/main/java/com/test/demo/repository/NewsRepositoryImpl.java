package com.test.demo.repository;

import com.test.demo.model.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NewsRepositoryImpl implements NewsRepository {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public List<News> getAllNews() {
        return mongoTemplate.findAll(News.class);
    }

    @Override
    public News getNewsById(String newsId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(newsId));

        return mongoTemplate.findOne(query, News.class);
    }

    @Override
    public News addNews(News news) {
        mongoTemplate.save(news);
        return news;
    }
}
