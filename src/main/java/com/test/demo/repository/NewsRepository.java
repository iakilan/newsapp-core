package com.test.demo.repository;

import com.test.demo.model.News;

import java.util.List;

public interface NewsRepository {

    /**
     *
     * @return
     */
    List<News> getAllNews();

    /**
     *
     * @param newsId
     * @return
     */
    News getNewsById(String newsId);

    /**
     *
     * @param news
     * @return
     */
    News addNews(News news);

}
