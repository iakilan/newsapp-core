package com.test.demo;

import com.test.demo.model.News;
import com.test.demo.repository.NewsRepositoryTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.Instant;
import java.util.Collections;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    @Autowired
    NewsRepositoryTest newsRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    // Added for initial dataload;
    @Override
    public void run(String... args) throws Exception {

        newsRepository.deleteAll();

        News news = new News();
        news.setUrl("http://player.vimeo.com/video/29193046");
        news.setProfileUrl("Some profile url");
        news.setContent("The quick brown fox jumps over the lazy dog.");
        news.setUserName("Justin Biber");
        news.setVote(1245L);

        News.Comment comment = new News.Comment();
        comment.setUserName("Ahilan");
        comment.setComment("This is worth to watch.....");
        comment.setVote(25L);
        comment.setPostedTime(Instant.now());

        news.setComments(Collections.singletonList(comment));

        newsRepository.save(news);
    }
}
