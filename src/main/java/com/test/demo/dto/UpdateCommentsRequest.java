package com.test.demo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
public class UpdateCommentsRequest {
    private String userName;
    private String comment;
    private Long vote;
    private Instant postedTime;
}
