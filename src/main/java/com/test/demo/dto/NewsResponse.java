package com.test.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewsResponse {
    private String id;
    private String url;
    private String profileUrl;
    private String name;
    private String content;
    private Long vote;
    private int commentCount;
}
