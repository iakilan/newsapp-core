package com.test.demo.controller;

import com.test.demo.dto.NewsResponse;
import com.test.demo.dto.UpdateCommentsRequest;
import com.test.demo.model.News;
import com.test.demo.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/")
public class NewsController {

    @Autowired
    private NewsService newsService;

    /**
     * @return
     */
    @GetMapping(value = "")
    public List<NewsResponse> getAllNews() {
        return newsService.getAllNews();
    }

 /*   @GetMapping(value = "/hash/{email}")
    public String mail(@PathVariable String email) {

        return Hashing.sha256().hashString(email, StandardCharsets.UTF_8).toString();
    }*/

    /**
     *
     * @param newsId
     * @return
     */
    @GetMapping(value = "/{newsId}")
    public News getNews(@PathVariable String newsId) {
        return newsService.getNewsById(newsId);
    }

    /**
     * @param news
     * @return
     */
    @PostMapping(value = "/create")
    public News addNewNews(@RequestBody News news) {
        return newsService.addNews(news);
    }

    /**
     * @param newsId
     * @param commentsRequest
     * @return
     */
    @PatchMapping(value = "/{newsId}/comment")
    public News updateComment(@PathVariable String newsId, @RequestBody UpdateCommentsRequest commentsRequest) {
        return newsService.updateComment(newsId, commentsRequest);
    }


}
